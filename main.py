import pyautogui
import win32gui
import re
import time


class WindowMgr:
    """Encapsulates some calls to the winapi for window management"""

    def __init__ (self):
        """Constructor"""
        self._handle = None

    def find_window(self, class_name, window_name=None):
        """find a window by its class_name"""
        self._handle = win32gui.FindWindow(class_name, window_name)

    def _window_enum_callback(self, hwnd, wildcard):
        """Pass to win32gui.EnumWindows() to check all the opened windows"""
        if re.match(wildcard, str(win32gui.GetWindowText(hwnd))) is not None:
            self._handle = hwnd

    def find_window_wildcard(self, wildcard):
        """find a window whose title matches the wildcard regex"""
        self._handle = None
        win32gui.EnumWindows(self._window_enum_callback, wildcard)

    def set_foreground(self):
        """put the window in the foreground"""
        # win32gui.SetForegroundWindow(self._handle)
        win32gui.SetActiveWindow(self._handle)

def px_to_perc(position, reverse=False):
    width, height = position
    display_size = pyautogui.size()
    if reverse:     # from Percentage to px
        conv_width = (display_size[0] * width) / 100
        conv_height = (display_size[1] * height) / 100
    else:
        conv_width = (width * 100) / display_size[0]
        cov_height = (height * 100) / display_size[1]
    conv_position = (conv_width, conv_height)
    return conv_position

def export_forces(VP, ses_nr, test):

    # pos File button
    h_file = 45
    w_file = 45
    h_export = 180
    w_export = 100
    w_ses1 = 800
    h_ses1 = 330
    delta_ses = 20

    pyautogui.click(w_file, h_file)
    pyautogui.click(w_export, h_export)
    time.sleep(.2)
    for ses in range(ses_nr + 1):
        pyautogui.click(w_ses1, h_ses1)
        time.sleep(.1)
        pyautogui.click(1180, 750)      # Export Button
        time.sleep(.2)
        pyautogui.click(1180, 770)      # Insert Filename
        time.sleep(.2)
        pyautogui.typewrite(f"0{VP}_0{test}_0{ses+1}_1")
        time.sleep(.2)
        pyautogui.click(1300, 850)      # Speichern
        time.sleep(.5)
        h_ses1 += delta_ses     # select next Session
    pyautogui.click(1100, 800)  # OK Button

def export_rev(VP, ses_nr, test):
    width = 40
    for ses in range(ses_nr + 1):
        pyautogui.click(width, 120)     #session
        time.sleep(.2)
        pyautogui.click(250, 45)        # Analyse Button
        time.sleep(.2)
        pyautogui.click(250, 190)      # Export analysis module
        time.sleep(.5)
        pyautogui.click(1100, 750)      # Export
        time.sleep(.8)
        pyautogui.click(1100, 770)      # Eingabezeile
        time.sleep(.2)
        pyautogui.typewrite(f"0{VP}_0{test}_0{ses+1}_2")
        time.sleep(.2)
        pyautogui.click(1380, 860)      # Speichern
        time.sleep(.8)
        pyautogui.click(1100, 800)      # OK Button
        time.sleep(.2)
        width += 72


if __name__ == '__main__':
    VP = int(input("Gib VP Code ein!"))
    ses_nr = int(input("Anzahl der Sessions?")) - 1
    test = 1
    w = WindowMgr()
    w.find_window_wildcard(".*WATTBIKE.*Expert.*")
    w.set_foreground()
    export_forces(VP, ses_nr, test)
    export_rev(VP, ses_nr, test)

    # pyautogui.moveTo(1380, 860)