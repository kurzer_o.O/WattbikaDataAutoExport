# Wattbike data auto export

Everyone how worked with the Wattbike Expert Software provided by Wattbike knows that exporting the force data as well as the Analysis Modul is a pain. Especially if you have to rename all files acording to a specific pattern.

This project is using pyautogui and win32gui to manage the magic.
It's a really basic sript you might need to customize to your needs.

## How it works

1. Start the Wattbike Expert Software (in fullscreen) and choose a Person
2. Satrt main.py 
3. If prompted type a subject name 
4. If prompted type the amount of sessions to export
5. Enjoy the magic

## Additional information

The program will ask for a subject name (VP) which will be included in the file name. I used just numbers.
It will also ask for the number of sessions to export. (for now you cant go from 4-5 only 1-x).
 Note, if you want to export 4 sessions insert 4.

In Line 66 and 86 you can create a file name as you wish for force data or analysis module, respectively.

The positions for autogui.click(x,y) are hard coded, you might want to adjust to your conditions. 
Im having a resolution of 1920 x 1080. So it should work if you have the same one.

Have fun!
